# chatBot
This program respond Hello World when you make a get request.

To install the program you have to clone it with the command line : git clone https://gitlab.com/victor.marignac/chatbot.git

After that, you have to run two command lines : npm init, npm install express --save

Then you just have to start the server with npm start

Once all this being done, you just have to run the command line : Curl http://localhost:3000/ to test it
